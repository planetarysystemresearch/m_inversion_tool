program main
    use mie
    use constants
    implicit none


    CHARACTER(len=32) :: arg,output_fname
    real(kind=rk), allocatable :: theta(:)   !theta angle
    real(kind=rk), allocatable :: S0(:,:)    !amplitude matrix elements
    real(kind=rk), allocatable :: P(:,:)     !scattering phase matrix
    complex(kind=rk), allocatable :: S(:,:)
    real(kind=rk) :: wavel, ka,a, mre, mim, mmed, vf, varpi, ell, sext, ssca, sabs, next(2),find,ell0
    integer :: nthe,over,j1
    
    mmed = 1.0_rk
    nthe = 180
    mim = 0.0_rk
    vf = 0.3
    mre = 1.0_rk

    call get_command_argument(1, arg) 
    read(arg,*) find
    call get_command_argument(2, arg) 
    read(arg,*) wavel
    call get_command_argument(3, arg) 
    read(arg,*) ka
    call get_command_argument(4, arg) 
    read(arg,*) ell0
    call get_command_argument(5, output_fname) 



    over = 0
    a = ka/(2*pi/wavel)

    do j1=1,100000
        !write(6,*) mre
        call getPSmatrix(wavel,a,mre,mim,mmed,vf,nthe,theta,P,S0,S,varpi,ell,sext,ssca,sabs)
        if(find>ssca) then
            mre=mre+0.0001_rk*exp(-1.0*over)
        else if(find<ssca) then
            mre=mre-0.0001_rk*exp(-1.0*over)
            over=over+1
        endif
    enddo


    write(6,*) mre,(ell0**(-1.0_rk))*2.0_rk*pi/wavel,ssca,find-ssca,output_fname
    open(unit=1,file=trim(adjustl(output_fname)))
        write(1,*) mre,(ell0**(-1.0_rk))/(2.0_rk*pi/wavel),ssca,find-ssca
        write(1,*) find,wavel,ka,ell0
    close(1)
end program
