!-----------------------------
!Generate spherical scatterers by using the Mie solution
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!-----------------------------
module constants
    use, intrinsic :: iso_fortran_env
    implicit none
    integer, parameter :: rk = REAL64                       !global precision(double)
    integer, parameter :: qp = REAL128
    real(kind=rk), parameter :: pi = 4.0_rk*atan(1.0_rk)    !pi
end module


module mie
    use constants
    implicit none
contains

    !----------------------------------------------------
    !subroutine getPSmatrix(wavel,a,mre,mim,mmed,vf,nthe,theta,M,S0,varpi,ell)
    !
    !Create data for splinePresentation by using the mie solution from Bohren and Huffman's book
    !----------------------------------------------------
    subroutine getPSmatrix(wavel,a,mre,mim,mmed,vf,nthe,theta,P,S0,S, varpi,ell,sext,ssca,sabs )
        integer, intent(in) :: nthe                             !number of scattering angles
        real(kind=rk), intent(in) :: wavel                      !wavelength
        real(kind=rk), intent(in) :: a                          !radius
        real(kind=rk), intent(in) :: mre,mim,mmed               !real, imag, medium refractive index
        real(kind=rk), intent(in) :: vf                         !volume faction
        real(kind=rk), allocatable, intent(inout) :: theta(:)   !theta angle
        real(kind=rk), allocatable, intent(inout) :: S0(:,:)    !amplitude matrix elements
        real(kind=rk), allocatable, intent(inout) :: P(:,:)     !scattering phase matrix
        real(kind=rk), intent(out) :: varpi                     !single scattering albedo
        real(kind=rk), intent(out) :: ell                       !mean free path
        real(kind=rk), intent(out) :: sext,ssca,sabs            !extincition, sca,abs cross sections

        complex(kind=rk), allocatable, intent(inout) :: S(:,:)
        real(kind=rk) :: waven,x,qext,qsca,qback,norm(2),asym(2),bin,kext
        
        real(kind=rk) :: sback,qabs,n
        complex(kind=rk) :: mrel
        integer :: j1
        mrel=cmplx(mre,mim,kind=rk)/mmed
        waven=2.0_rk*pi*mmed/wavel
        x=waven*a
        qext=0.0_rk
        qsca=0.0_rk
        qback=0.0_rk
        if(.not. allocated(P)) allocate(P(4,nthe+1),S(2,nthe+1),theta(nthe+1),S0(4,nthe+1))
        call mieSolution(x,mrel,nthe/2+1,S(1,:),S(2,:),qext,qsca,qback)
 
        qabs=qext-qsca    
        sext=qext*pi*a**2
        ssca=qsca*pi*a**2
        sabs=qabs*pi*a**2
        sback=qback*pi*a**2
    end subroutine


    !----------------------------------------------------
    !subroutine savePSToFile(theta,M,S,additionalText,filename,writeAddionalText)
    !
    !Save data created by mie module to later use.
    !Data will be saved into "filename" in a format: theta angle, Phase, Amplitude
    !If writeAddionalText=.true. , additional text will be added at the beginning of
    !the file
    !----------------------------------------------------
    subroutine savePSToFile(theta,P,S,additionalText,filename,writeAdditionalText)
        real(kind=rk), intent(in) :: theta(:)           !theta angle
        real(kind=rk), intent(in) :: P(:,:)             !Scattering phase matrix
        real(kind=rk), intent(in) :: S(:,:)             !Amplitude matrix elements
        character(len=*), intent(in) :: additionalText  !additional text
        character(len=*), intent(in) :: filename        !filename 
        logical, intent(in) :: writeAdditionalText        !if true write addional text
        integer :: j1,stream
        stream=123456
        open(unit=stream,file=adjustl(trim(filename)))
        if(writeAdditionalText) write(stream,*) "#",adjustl(trim(additionalText))
        do j1=1,size(theta)
            write(stream,*) theta(j1),P(:,j1),S(:,j1) 
        enddo
        close(stream)
    end subroutine

    !----------------------------------------------------
    !subroutine mieSolution(x,mrel,nang,s1,s2,qext,qsca,qback)
    !Solve amplitude scattering matrix of the spherical scatterer
    !
    !Original: BHMIE by Bohren and Huffman (1983).
    !Modified to accept arrays of any size and modernized.
    !----------------------------------------------------
    subroutine mieSolution(x,mrel,nang,s1,s2,qext,qsca,qback)
        real(kind=rk),intent(inout) :: qext,qsca,qback      !efficiencies
        complex(kind=rk), intent(inout) :: s1(:),s2(:)      !amplitude scattering matrix elements
        complex(kind=rk), intent(in) :: mrel                !relative refractive index
        real(kind=rk), intent(inout) :: x                   !size parameter of the scatterer
        integer, intent(in) :: nang                         !number of scattering angles

        complex(kind=rk) :: y
        real(kind=rk) :: xstop,norm(2),asym(2),psi,p
        real(kind=rk),allocatable :: amu(:),theta(:),pii(:),tau(:),pi0(:),pi1(:)
        complex(kind=rk), allocatable :: d(:)
        integer :: j1,j2,n,jj,nn,nstop,nmx
        real(kind=rk) :: apsi,apsi1,apsi0,chi,chi1
        real(kind=rk) :: fn,chi0,psi0,psi1,rn,t,ymod,dang
        complex(kind=rk) xi,xi0,xi1,bn,an
        y=x*mrel
        xstop=x+4.0_rk*x**(1.0_rk/3.0_rk)+2.0_rk
        nstop=int(xstop)
        ymod=abs(y)
        nmx=max(xstop,ymod)+15
        dang=pi/(2.0_rk*(nang-1.0_rk))
        allocate(theta(nang),amu(nang))
        allocate(pi0(nang),pi1(nang),pii(nang))
        allocate(d(nmx),tau(nang))
        do j1=1,nang
            theta(j1)=(j1-1)*dang
            amu(j1)=cos(theta(j1))
        enddo

        d(nmx)=cmplx(0.0_rk,0.0_rk,kind=rk)
        nn=nmx-1
        do n=1,nn
            rn=nmx-n+1
            d(nmx-n)=(rn/y)-(1.0_rk/(d(nmx-n+1)+rn/y))
        enddo
        
        pi0(:)=0.0_rk
        pi1(:)=1.0_rk
        s1(:)=0.0_rk
        s2(:)=0.0_rk
        
        psi0=cos(x)
        psi1=sin(x)
        chi0=-sin(x)
        chi1=cos(x)
        apsi0=psi0
        apsi1=psi1
        xi0=cmplx(apsi0,-chi0,kind=rk)
        xi1=cmplx(apsi1,-chi1,kind=rk)
        qsca=0.0_rk
        
        do n=1,nstop
            rn=n
            fn=(2.0_rk*rn+1.0_rk)/(rn*(rn+1))
            psi=(2.0_rk*rn-1.0_rk)*psi1/x-psi0
            apsi=psi
            chi=(2.0_rk*rn-1.0_rk)*chi1/x-chi0
            xi=cmplx(apsi,-chi,kind=rk)
            an=(d(n)/mrel+rn/x)*apsi-apsi1
            an=an/((d(n)/mrel+rn/x)*xi-xi1)
            bn=(mrel*d(n)+rn/x)*apsi-apsi1
            bn=bn/((mrel*d(n)+rn/x)*xi-xi1)
            qsca=qsca+(2.0_rk*rn+1.0_rk)*(abs(an)**2.0_rk+abs(bn)**2.0_rk)
            do j1=1,nang
                jj=2*nang-j1
                pii(j1)=pi1(j1)
                tau(j1)=rn*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)
                p=(-1.0_rk)**(n-1)
                s1(j1)=s1(j1)+fn*(an*pii(j1)+bn*tau(j1))
                t=(-1.0_rk)**n
                s2(j1)=s2(j1)+fn*(an*tau(j1)+bn*pii(j1))
                if (j1/=jj) then
                    s1(jj)=s1(jj)+fn*(an*pii(j1)*p+bn*tau(j1)*t)
                    s2(jj)=s2(jj)+fn*(an*tau(j1)*t+bn*pii(j1)*p)
                endif
            enddo
            
            psi0=psi1
            psi1=psi
            apsi1=psi1
            chi0=chi1
            chi1=chi
            xi1=cmplx(apsi1,-chi1,kind=rk)
            
            do j1=1,nang
                pi1(j1)=((2.0_rk*rn+1.0_rk)/(rn))*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)/rn
                pi0(j1)=pii(j1)
            enddo
        enddo

        qsca=2.0_rk/x**2*qsca
        qext=4.0_rk/x**2*real(s1(1),kind=rk)
        qback=4.0_rk/x**2*abs(s1(2*nang-1))**2

        deallocate(theta,amu)
        deallocate(pi0,pi1,pii)
        deallocate(d,tau)
    end subroutine

end module 
