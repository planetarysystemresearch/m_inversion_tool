
       program PMDEC
 
! Program PMDE! presents the scattering phase matrix elements using cubi! 
! splines via Nelder-Mead simplex optimization. It provides decompositions 
! into pure scattering matrices.
!       
! Author: Karri Muinonen
! Version: 2019 January 10

       implicit none
       integer nthe,np,ndat,nint,j0,j1,j2,j3,j4,pflg
       double precision F0(0:360,4,4),F0E(0:360,4,4)
       double precision THE0(0:360),F01(0:360),F01E(0:360)
       double precision PCF(0:360,4,4),PCF1(10,0:360),PCF2(0:360,4,4)
       double precision  PC(0:360,4,4),THEC(0:360),PC1(0:360),PC2(0:360),PC3(0:360)
       double precision CHISQ(10),RMS(10)
       double precision ftol,dthe,pnorm,w,p11,p12r,p22r,p33r,p34r,p44r
       double precision pi,rd,tmp0
        character(len=128) :: arg,fname,tmp1
       integer jdels
       double precision PUV(10,0:360,4,4),THEUV(0:360)
       double precision pnorm1,pnorm2,wu,s1pls,s1mns,dels,chi2,s2,t1,t2
       double complex S1(0:360,2),SUV(10,0:360,4)
       integer irn,ncount
       double precision tmp
        logical :: full


       pi=4.0d0*atan(1.0d0)
       rd=pi/180.0d0

       
!     Input scattering phase matrix data:

!       ndat=38   ! Extrapolated backscattering and forward scattering, incoherent. ! Feldspar 442
!       ndat=36   ! Extrapolated backscattering and forward scattering. ! Feldspar 633
!       ndat=36  ! Extrapolated backscattering and forward scattering ! Olivine S
!       ndat=46   ! Extrapolated backscattering and forward scattering. ! Spurr-Ashton 633
!       ndat=180   ! Extrapolated backscattering and forward scattering. ! SIRIS3

        call get_command_argument(1, fname)
        call get_command_argument(2, arg)
        read(arg,*) ndat
   

        ncount = command_argument_count()
        if(ncount==2) then
            full = .true.
        else
            call get_command_argument(3, tmp1)
            read(tmp1,*) full
        endif


       do 30 j1=0,ndat
        do 20 j2=1,4
         do 10 j3=1,4
          F0(j1,j2,j3)=0.0d0
          F0E(j1,j2,j3)=0.0d0
          PCF2(j1,j2,j3)=0.0d0
10       continue
20      continue
30     continue

       

        open(unit=1,file=trim(adjustl(fname)),status='old')
        do 40 j1=0,ndat
                if(full) then
                        read (1,*) THE0(j1),F0(j1,1,1),F0(j1,1,2),tmp,tmp, &
                                &           tmp,F0(j1,2,2),tmp,tmp,        &  
                                &           tmp,tmp,F0(j1,3,3),F0(j1,3,4), &
                                &           tmp,tmp,tmp,F0(j1,4,4)      
                else
                        read (1,*) THE0(j1),F0(j1,1,1),F0(j1,1,2), &
                        &                   F0(j1,2,2),F0(j1,3,3), &
                        &                   F0(j1,3,4),F0(j1,4,4)   
                endif  
                THE0(j1)=THE0(j1)*rd
                tmp0 = F0(j1,1,1)
                F0(j1,:,:) = F0(j1,:,:)/tmp0 
                F0(j1,1,1)=tmp0
                F0(j1,1,2) = - F0(j1,1,2) 
                F0(j1,2,1)=F0(j1,1,2)
                F0(j1,4,3)=-F0(j1,3,4)
                !print*,j2,F0(j1,1,1),F0(j1,1,2),F0(j1,2,2), &
                !&            F0(j1,3,3),F0(j1,3,4),F0(j1,4,4)
40     continue
       close(unit=1)
 
! Initial parameters and uniform angular grid:
  
       nint=128   ! 64
       ftol=1.0d-8

       nthe=180
       dthe=pi/nthe
       do 50 j1=0,nthe
        THEUV(j1)=j1*dthe
50     continue
     
! Initialize spline presentation to be computed:

       np=15
       do 120 j1=0,np
        do 110 j2=1,4
         do 100 j3=1,4
          PC(j1,j2,j3)=0.0d0
100      continue
110     continue
120    continue

       THEC(0)=0.0d0
       THEC(1)=8.0d0*rd
       THEC(2)=12.0d0*rd
       THEC(3)=16.0d0*rd
       THEC(4)=20.0d0*rd
       do 130 j1=1,7
        THEC(4+j1)=(j1+1)*20.0d0*rd
130    continue
       THEC(np-3)=164.0d0*rd
       THEC(np-2)=168.0d0*rd
       THEC(np-1)=172.0d0*rd
       THEC(np)=pi

       PC(0,1,1)=1.1d0*F0(0,1,1)   ! Observe initial choice.
!       PC(0,1,1)=F0(0,1,1)   ! Observe initial choice.
       PC(np,1,1)=F0(ndat,1,1)

       PC(0,1,2)=0.0d0
       PC(np,1,2)=0.0d0

       PC(0,2,2)=1.0d0
       PC(np,2,2)=1.0d0*F0(ndat,2,2)  ! Obs!

       PC(0,3,3)=1.0d0
       PC(np,3,3)=-PC(np,2,2)

       PC(0,3,4)=0.0d0
       PC(np,3,4)=0.0d0

       PC(0,4,4)=1.0d0
       PC(np,4,4)=1.0d0-2.0d0*PC(np,2,2)

       do 170 j1=1,np-1
        do 160 j2=1,ndat
         if (THE0(j2).gt.THEC(j1)) then
          w=(THE0(j2)-THEC(j1))/(THE0(j2)-THE0(j2-1))
          do 150 j3=1,4
           do 140 j4=1,4
            PC(j1,j3,j4)=w*F0(j2-1,j3,j4)+(1.0d0-w)*F0(j2,j3,j4)
140        continue
150       continue
          goto 170
         endif
160     continue 
170    continue
 
       do 180 j1=0,np
        PC1(j1)=PC(j1,1,1)
180    continue
       call PNRM(PC1,THEC,pnorm,np,nint)
       do 190 j1=0,np
        PC(j1,1,1)=PC(j1,1,1)*2.0d0/pnorm
!        print*,THEC(j1)/rd,PC(j1,1,1)
190    continue

! Initial normalization of the scattering phase function data:

       do 200 j1=0,ndat
        F0(j1,1,1)=F0(j1,1,1)*2.0d0/pnorm
!        print*,THE0(j1)/rd,F0(j1,1,1)
200    continue

       print*,'Norm, initial   ',2.0d0/pnorm

! Optimize P11, final normalization of the scattering phase function:

       pflg=1
       do 300 j1=0,ndat
        F01(j1)=F0(j1,1,1)
!        F01E(j1)=F0E(j1,1,1)
        F01E(j1)=F01(j1)
300    continue
       do 310 j1=1,np+1
        PC1(j1-1)=PC(j1-1,1,1)
310    continue
       print*,'pflg, niter, chisq     '
       call NMOPT(PCF1,PC1,PC2,THEC,F01,F01E,THE0,CHISQ,RMS,ftol, &
     &            nthe,np,ndat,pflg)
       call PNRM(PC1,THEC,pnorm,np,nint)
       print*,'Norm, final     ',2.0d0/pnorm
       do 320 j1=1,np+1
        PCF(j1-1,1,1)=PC1(j1-1)*2.0d0/pnorm
320    continue
       do 330 j1=0,nthe
          PCF1(pflg,j1)=PCF1(pflg,j1)*2.0d0/pnorm
!          print*,THEUV(j1)/rd,PCF1(pflg,j1)
330    continue
       do 340 j1=0,ndat
        PCF2(j1,1,1)=PC2(j1)*2.0d0/pnorm
        F0(j1,1,1)=F0(j1,1,1)*2.0d0/pnorm
!        print*,THE0(j1)/rd,F0(j1,1,1),PCF2(j1,1,1)
340    continue
      
       print*,'pflg, niter, chisq     '

! Optimize P12:

       pflg=2
       do 400 j1=0,ndat
        F01(j1)=F0(j1,1,2)
!        F01E(j1)=F0E(j1,1,2)
        F01E(j1)=1.0d0
400    continue
       do 410 j1=1,np+1
        PC1(j1-1)=PC(j1-1,1,2)
410    continue
       call NMOPT(PCF1,PC1,PC2,THEC,F01,F01E,THE0,CHISQ,RMS,ftol, &
     &            nthe,np,ndat,pflg)
       do 420 j1=1,np+1
        PCF(j1-1,1,2)=PC1(j1-1)
        PCF(j1-1,2,1)=PC1(j1-1)
420    continue
       do 430 j1=0,ndat
        PCF2(j1,1,2)=PC2(j1)
        PCF2(j1,2,1)=PC2(j1)
!        print*,THE0(j1)/rd,F01(j1),PC2(j1)
430    continue

! Optimize P22:

       pflg=3
       do 500 j1=0,ndat
        F01(j1)=F0(j1,2,2)
!        F01E(j1)=F0E(j1,2,2)
        F01E(j1)=1.0d0
500    continue
       do 510 j1=1,np+1
        PC1(j1-1)=PC(j1-1,2,2)
510    continue
       call NMOPT(PCF1,PC1,PC2,THEC,F01,F01E,THE0,CHISQ,RMS,ftol, &
     &            nthe,np,ndat,pflg)
       do 520 j1=1,np+1
        PCF(j1-1,2,2)=PC1(j1-1)
520    continue
       do 530 j1=0,ndat
        PCF2(j1,2,2)=PC2(j1)
!        print*,THE0(j1)/rd,F01(j1),PC2(j1)
530    continue

! Optimize P33:

 535   pflg=4
       do 600 j1=0,ndat
        F01(j1)=F0(j1,3,3)
!        F01E(j1)=F0E(j1,3,3)
        F01E(j1)=1.0d0
600    continue
       do 610 j1=1,np+1
        PC1(j1-1)=PC(j1-1,3,3)
610    continue
       call NMOPT(PCF1,PC1,PC2,THEC,F01,F01E,THE0,CHISQ,RMS,ftol, &
     &            nthe,np,ndat,pflg)
       do 620 j1=1,np+1
        PCF(j1-1,3,3)=PC1(j1-1)
620    continue
       do 630 j1=0,ndat
        PCF2(j1,3,3)=PC2(j1)
!        print*,THE0(j1)/rd,F01(j1),PC2(j1)
630    continue

! Optimize P34:

       pflg=5
       do 700 j1=0,ndat
        F01(j1)=F0(j1,3,4)
!        F01E(j1)=F0E(j1,3,4)
        F01E(j1)=1.0d0
700    continue
       do 710 j1=1,np+1
        PC1(j1-1)=PC(j1-1,3,4)
710    continue
       call NMOPT(PCF1,PC1,PC2,THEC,F01,F01E,THE0,CHISQ,RMS,ftol, &
     &            nthe,np,ndat,pflg)
       do 720 j1=1,np+1
        PCF(j1-1,3,4)=PC1(j1-1)
        PCF(j1-1,4,3)=-PC1(j1-1)
720    continue
       do 730 j1=0,ndat
        PCF2(j1,3,4)=PC2(j1)
        PCF2(j1,4,3)=-PC2(j1)
!        print*,THE0(j1)/rd,F01(j1),PC2(j1)
730    continue

! Optimize P44:

       pflg=6
       do 800 j1=0,nthe
        F01(j1)=F0(j1,4,4)
!        F01E(j1)=F0E(j1,4,4)
        F01E(j1)=1.0d0
800    continue
       do 810 j1=1,np+1
        PC1(j1-1)=PC(j1-1,4,4)
810    continue
       call NMOPT(PCF1,PC1,PC2,THEC,F01,F01E,THE0,CHISQ,RMS,ftol, &
     &            nthe,np,ndat,pflg)
       do 820 j1=1,np+1
        PCF(j1-1,4,4)=PC1(j1-1)
820   continue
       do 830 j1=0,ndat
        PCF2(j1,4,4)=PC2(j1)
!        print*,THE0(j1)/rd,F01(j1),PC2(j1)
830    continue

       
! Output of P11, -P12/P11, P22/P11, P33/P11, P34/P11, P44/P11:

       open (unit=1,file='pmdeca.out') ! Values at the spline nodes.
       do 900 j1=0,np
        write(1,910) THEC(j1)/rd,PCF(j1,1,1),PCF(j1,1,2),PCF(j1,2,2), &
     &                           PCF(j1,3,3),PCF(j1,3,4),PCF(j1,4,4)
900    continue
910    format(F7.2,12(1X,E14.8))
       close (unit=1)

       open (unit=1,file='pmdecb.out') ! Values at the input data angles.
       do 920 j1=0,ndat
        write(1,910) THE0(j1)/rd,PCF2(j1,1,1),PCF2(j1,1,2),PCF2(j1,2,2), &
     &                           PCF2(j1,3,3),PCF2(j1,3,4),PCF2(j1,4,4)
920    continue
       close (unit=1)

       open (unit=1,file='pmdecc.out') ! O-! residuals, rms, chi2.
       do 930 j1=0,ndat
        write(1,910) THE0(j1)/rd,F0(j1,1,1)-PCF2(j1,1,1), &
     &                           F0(j1,1,2)-PCF2(j1,1,2), &
     &                           F0(j1,2,2)-PCF2(j1,2,2), &
     &                           F0(j1,3,3)-PCF2(j1,3,3), &
     &                           F0(j1,3,4)-PCF2(j1,3,4), &
     &                           F0(j1,4,4)-PCF2(j1,4,4)
930    continue
       write (1,940) (RMS(j1),j1=1,6)
       write (1,940) (CHISQ(j1),j1=1,6)
940    format(7X,6(1X,E14.8))
       close (unit=1)
       
       open (unit=1,file='pmdecd.out') ! Values for a uniform grid of angles.
       do 950 j1=0,nthe
        write(1,910) j1*dthe/rd,(PCF1(j2,j1),j2=1,6)
950    continue
       close (unit=1)

       open (unit=1,file='pmdec0.out')  ! Initial pure Mueller matrix for a uniform grid.
       open (unit=2,file='smdec0.out')  ! Initial pure Jones matrix for a uniform grid.
       do 960 j1=0,nthe
        if (j1.le.nthe/2) then
         p33r= sqrt(1.0d0-PCF1(2,j1)**2-PCF1(5,j1)**2)
        else         
         !write(6,*) 1.0d0-PCF1(2,j1)**2-PCF1(5,j1)**2
         if(1.0d0-PCF1(2,j1)**2-PCF1(5,j1)**2>=0.0d0) then
           p33r=-sqrt(1.0d0-PCF1(2,j1)**2-PCF1(5,j1)**2)
         else
           write(6,*) "WARNING, CAREFUL HERE"
           p33r = 0.0d0
         endif
        endif
        write(1,910) j1*dthe/rd,PCF1(1,j1),PCF1(2,j1),p33r,PCF1(5,j1)

        S1(j1,1)=dcmplx(sqrt(PCF1(1,j1)+PCF1(1,j1)*PCF1(2,j1)),0.0d0)
        S1(j1,2)=PCF1(1,j1)*dcmplx(p33r,PCF1(5,j1))/ &
     &           dreal(S1(j1,1))
        write(2,910) j1*dthe/rd,dreal(S1(j1,1)),dimag(S1(j1,1)), &
     &                          dreal(S1(j1,2)),dimag(S1(j1,2))
960    continue
       close (unit=2)
       close (unit=1)
       

! Normalization of co-polarized and cross-polarized phase functions, 
! weight factor for the co-polarized part:      

       do 1000 j1=0,nthe
        PC1(j1)=0.5d0*(1.0d0+PCF1(3,j1))*PCF1(1,j1)
        PC2(j1)=0.5d0*(1.0d0-PCF1(3,j1))*PCF1(1,j1)
        PC3(j1)=PCF1(1,j1)
1000   continue
       call PNRM(PC1,THEUV,pnorm1,nthe,nint)
       call PNRM(PC2,THEUV,pnorm2,nthe,nint)
       call PNRM(PC3,THEUV,pnorm,nthe,nint)
       wu=pnorm1/(pnorm1+pnorm2)
       print*,'Norms, total         ',pnorm,pnorm1+pnorm2
       print*,'Norms, co and cross  ',pnorm1,pnorm2
       print*,'Weight factor wu     ', wu
       
       do 1010 j1=0,nthe
        PC1(j1)=PC1(j1)*2.0d0/pnorm1
        PC2(j1)=PC2(j1)*2.0d0/pnorm2
1010   continue

! Polarization scaling factor dels for U1+ and U1-:       
       
       dels=0.0d0
       do 1020 j1=0,nthe
        PC3(j1)=2.0d0*sqrt(PCF1(2,j1)**2+PCF1(5,j1)**2)/ &
     &          (1.0d0+PCF1(3,j1))
        if (PC3(j1).ge.dels) then
         dels=PC3(j1)
         jdels=j1
        endif
1020   continue

! Reset scattering phase and amplitude matrices:        

       do 1060 j0=1,10 
        do 1050 j1=0,nthe 
         do 1040 j2=1,4
          SUV(j0,j1,j2)=dcmplx(0.0d0,0.0d0)
          do 1030 j3=1,4
           PUV(j0,j1,j2,j3)=0.0d0
1030      continue
1040     continue
1050    continue
1060   continue


! U1+, U1-:

       do 1100 j1=0,nthe

        PUV(1,j1,1,1)=PC1(j1)
        PUV(1,j1,1,2)=-2.0d0*PCF1(2,j1)/(1.0d0+PCF1(3,j1))/dels* &
     &                  PUV(1,j1,1,1)
        PUV(1,j1,2,1)=PUV(1,j1,1,2)
        PUV(1,j1,2,2)=PUV(1,j1,1,1)
        PUV(1,j1,3,4)= 2.0d0*PCF1(5,j1)/(1.0d0+PCF1(3,j1))/dels* &
     &                  PUV(1,j1,1,1)
        if (j1.le.jdels) then
         PUV(1,j1,3,3)= min(PUV(1,j1,1,1)**2, &
     &          PUV(1,j1,1,1)**2-PUV(1,j1,1,2)**2-PUV(1,j1,3,4)**2)
         IF(PUV(1,j1,3,3)<0.0D0)   WRITE(6,*) "warning!",J1,PUV(1,j1,3,3)
         PUV(1,j1,3,3)=sqrt(MAX(0.0D0,PUV(1,j1,3,3)))
        else
         PUV(1,j1,3,3)=-max(-PUV(1,j1,1,1)**2, &
     &          PUV(1,j1,1,1)**2-PUV(1,j1,1,2)**2-PUV(1,j1,3,4)**2)
         PUV(1,j1,3,3)=-sqrt(abs(PUV(1,j1,3,3)))         
        endif        
        PUV(1,j1,4,3)=-PUV(1,j1,3,4)
        PUV(1,j1,4,4)= PUV(1,j1,3,3) 
!        print*,j1,PUV(1,j1,3,3)/PUV(1,j1,1,1)
        
        PUV(2,j1,1,1)= PUV(1,j1,1,1)
        PUV(2,j1,1,2)=-PUV(1,j1,1,2)
        PUV(2,j1,2,1)=-PUV(1,j1,2,1)
        PUV(2,j1,2,2)= PUV(1,j1,2,2)
        PUV(2,j1,3,3)= PUV(1,j1,3,3) 
        PUV(2,j1,3,4)=-PUV(1,j1,3,4) 
        PUV(2,j1,4,3)=-PUV(1,j1,4,3) 
        PUV(2,j1,4,4)= PUV(1,j1,3,3) 

        SUV(1,j1,1)=dcmplx(sqrt(PUV(1,j1,1,1)+PUV(1,j1,1,2)),0.0d0)
        SUV(1,j1,2)=dcmplx(PUV(1,j1,3,3),PUV(1,j1,3,4))/dreal(SUV(1,j1,1))
        SUV(2,j1,1)=dcmplx(sqrt(PUV(2,j1,1,1)+PUV(2,j1,1,2)),0.0d0)
        SUV(2,j1,2)=dcmplx(PUV(2,j1,3,3),PUV(2,j1,3,4))/dreal(SUV(2,j1,1))
1100   continue
       
! U2+, U2-:

       s2=0.5d0*(1.0d0-PCF1(3,nthe))/(1.0d0+PCF1(3,nthe))
       s1pls=0.5d0*(1.0d0+dels-2.0d0*s2)
       s1mns=0.5d0*(1.0d0-dels-2.0d0*s2)

       print*,'jds, ds, 1-ds ',jdels,dels,1.0d0-dels
       print*,'s1pls, s1mns  ',s1pls,s1mns
       print*,'s2            ',s2
       print*,'              '
       print*,'jthe, abs(U2_33/U2_11) > 1, regularization done'

       do 1110 j1=0,nthe
        PUV(3,j1,1,1)=PUV(1,j1,1,1)
        PUV(3,j1,1,2)=0.0d0
        PUV(3,j1,2,1)=PUV(3,j1,1,2)
        PUV(3,j1,2,2)=PUV(3,j1,1,1)

        PUV(3,j1,3,3)= (0.5d0*PCF1(1,j1)*(PCF1(4,j1)+PCF1(6,j1))/&
     &   wu-(1.0d0-2.0d0*s2)*PUV(1,j1,3,3))/(2.0d0*s2)
        if (abs(PUV(3,j1,3,3)).gt.abs(PUV(3,j1,1,1))) then
         print*,j1,PUV(3,j1,3,3)/PUV(3,j1,1,1)
         PUV(3,j1,3,3)=sign(1.0d0,PUV(3,j1,3,3))*PUV(3,j1,1,1)
        endif
        PUV(3,j1,3,4)= sqrt(PUV(3,j1,1,1)**2-PUV(3,j1,3,3)**2)
        PUV(3,j1,4,3)=-PUV(3,j1,3,4)
        PUV(3,j1,4,4)= PUV(3,j1,3,3) 

        PUV(4,j1,1,1)= PUV(3,j1,1,1)
        PUV(4,j1,1,2)=-PUV(3,j1,1,2)
        PUV(4,j1,2,1)=-PUV(3,j1,2,1)
        PUV(4,j1,2,2)= PUV(3,j1,2,2)
        
        PUV(4,j1,3,3)= PUV(3,j1,3,3) 
        PUV(4,j1,3,4)=-PUV(3,j1,3,4) 
        PUV(4,j1,4,3)=-PUV(3,j1,4,3) 
        PUV(4,j1,4,4)= PUV(3,j1,3,3) 

        SUV(3,j1,1)=dcmplx(sqrt(PUV(3,j1,1,1)+PUV(3,j1,1,2)),0.0d0)
        SUV(3,j1,2)=dcmplx(PUV(3,j1,3,3),PUV(3,j1,3,4))/dreal(SUV(3,j1,1))
        SUV(4,j1,1)=dcmplx(sqrt(PUV(4,j1,1,1)+PUV(4,j1,1,2)),0.0d0)
        SUV(4,j1,2)=dcmplx(PUV(4,j1,3,3),PUV(4,j1,3,4))/dreal(SUV(4,j1,1))
1110   continue
       
! V1+, V1- (currently identical matrices and not utilized):

       t1=0.0d0
       do 1120 j1=0,nthe
        PUV(5,j1,1,1)=PC2(j1)
        PUV(5,j1,2,2)=-PUV(5,j1,1,1)
        PUV(5,j1,3,3)= PUV(5,j1,1,1)
        PUV(5,j1,4,4)=-PUV(5,j1,1,1)
        PUV(6,j1,1,1)=PUV(5,j1,1,1)
        PUV(6,j1,2,2)=PUV(5,j1,2,2)
        PUV(6,j1,3,3)=PUV(5,j1,3,3)
        PUV(6,j1,4,4)=PUV(5,j1,4,4)

        if (j1.ne.0) then
         SUV(5,j1,3)=dcmplx(sqrt(PUV(5,j1,1,1)),0.0d0)
         SUV(5,j1,4)=dcmplx(PUV(5,j1,3,3),0.0d0)/dreal(SUV(5,j1,3))
         SUV(6,j1,3)=dcmplx(sqrt(PUV(6,j1,1,1)),0.0d0)
         SUV(6,j1,4)=dcmplx(PUV(6,j1,3,3),0.0d0)/dreal(SUV(6,j1,3))
        else
         SUV(5,j1,3)=0.0d0
         SUV(5,j1,4)=0.0d0
         SUV(6,j1,3)=0.0d0
         SUV(6,j1,4)=0.0d0
        endif
1120   continue

! V2+, V2-:

       t2=0.5d0
       print*,'              '
       print*,'t1, t2        ',t1,t2
       print*,'              '
       print*,'jthe, abs(V2_33/V2_11) > 1, regularization done'
       do 1130 j1=0,nthe
        PUV(7,j1,1,1)= PUV(5,j1,1,1)
        PUV(7,j1,2,2)=-PUV(7,j1,1,1)
        PUV(7,j1,3,3)=0.5d0*PCF1(1,j1)*(PCF1(4,j1)-PCF1(6,j1))/&
     &       (1.0d0-wu)
        if (abs(PUV(7,j1,3,3)).gt.abs(PUV(7,j1,1,1))) then
         print*,j1,PUV(7,j1,3,3)/PUV(7,j1,1,1)
         PUV(7,j1,3,3)=sign(1.0d0,PUV(7,j1,3,3))*PUV(7,j1,1,1)
        endif
        PUV(7,j1,3,4)= sqrt(PUV(7,j1,1,1)**2-PUV(7,j1,3,3)**2)
        PUV(7,j1,4,3)= PUV(7,j1,3,4) 
        PUV(7,j1,4,4)=-PUV(7,j1,3,3) 

        PUV(8,j1,1,1)= PUV(7,j1,1,1)
        PUV(8,j1,2,2)=-PUV(7,j1,1,1)
        PUV(8,j1,3,3)= PUV(7,j1,3,3)
        PUV(8,j1,3,4)=-PUV(7,j1,3,4) 
        PUV(8,j1,4,3)= PUV(7,j1,3,4) 
        PUV(8,j1,4,4)=-PUV(8,j1,3,3)

        if (j1.ne.0) then
         SUV(7,j1,3)=dcmplx(sqrt(PUV(7,j1,1,1)),0.0d0)
         SUV(7,j1,4)=dcmplx(PUV(7,j1,3,3),PUV(7,j1,3,4))/&
     &               dreal(SUV(7,j1,3))
         SUV(8,j1,3)=dcmplx(sqrt(PUV(8,j1,1,1)),0.0d0)
         SUV(8,j1,4)=dcmplx(PUV(8,j1,3,3),PUV(8,j1,3,4))/&
     &               dreal(SUV(8,j1,3))
        else
         SUV(7,j1,3)=0.0d0
         SUV(7,j1,4)=0.0d0
         SUV(8,j1,3)=0.0d0
         SUV(8,j1,4)=0.0d0
        endif
1130   continue

       open (unit=1,file='pmodel.out')  
       do 1200 j1=0,nthe
        p11 =wu*PUV(1,j1,1,1)+(1.0d0-wu)*PUV(5,j1,1,1)          
        p12r=-wu*(s1pls-s1mns)*PUV(1,j1,1,2)/p11
        p22r=(wu*PUV(1,j1,2,2)+(1.0d0-wu)*PUV(5,j1,2,2))/p11
        p34r= wu*(s1pls-s1mns)*PUV(1,j1,3,4)/p11
        p33r=(wu*((s1pls+s1mns)*PUV(1,j1,3,3)+2.0d0*s2*PUV(3,j1,3,3))+&
     &       (1.0d0-wu)*PUV(7,j1,3,3))/p11
        p44r=(wu*((s1pls+s1mns)*PUV(1,j1,4,4)+2.0d0*s2*PUV(3,j1,4,4))+&
     &       (1.0d0-wu)*PUV(7,j1,4,4))/p11
        write(1,1210) THEUV(j1)/rd,p11,p12r,p22r,p33r,p34r,p44r
1200   continue
       close (unit=1)
1210   format(F7.2,50(1X,E14.8))
1220   format(12(1X,E14.8))

! Pure Mueller matrices and the corresponding Jones matrices for a uniform grid:
       
       open (unit=1,file='puv.out') 
       open (unit=2,file='suv.out')  
       do 1230 j1=0,nthe
        write(1,1210) THEUV(j1)/rd,&
     &     (PUV(j2,j1,1,1),PUV(j2,j1,1,2),PUV(j2,j1,2,1),PUV(j2,j1,2,2),&
     &      PUV(j2,j1,3,3),PUV(j2,j1,3,4),PUV(j2,j1,4,3),PUV(j2,j1,4,4),&
     &      j2=1,8)
        write(2,1210) THEUV(j1)/rd, &
     &             (dreal(SUV(j2,j1,1)),dimag(SUV(j2,j1,1)), &
     &              dreal(SUV(j2,j1,2)),dimag(SUV(j2,j1,2)), &
     &              dreal(SUV(j2,j1,3)),dimag(SUV(j2,j1,3)), &
     &              dreal(SUV(j2,j1,4)),dimag(SUV(j2,j1,4)), &
     &              j2=1,8)
1230   continue
       write (1,1220) wu,s1pls,s1mns,s2,s2,t1,t1,t2,t2
       close (unit=2)
       close (unit=1)
       end
 


       subroutine NMOPT(PCF1,PC1,PC2,THEC,F01,F01E,THE0,CHISQ,RMS,ftol, &
     &                  nthe,np,ndat,pflg)

! Nelder-Mead downhill simplex optimization of the spline model against 
! the input data.
!
! Author: Karri Muinonen
! Version: 2019 January 7

       implicit none
       integer nthe,np,ndat,pflg,j1,j2
       double precision PCF1(10,0:360),PC1(0:360),PC2(0:360)
       double precision  THEC(0:360),F01(0:360),F01E(0:360),THE0(0:360)
       double precision CHISQ(10),RMS(10),q,pi,rd
       double precision XP(361),YPA(361),YPA2(361)

       integer ndim,iter
       double precision PLS(50),F0(50),DP(50),PP(50)
       double precision   P(51,50),Y(51),CHI2PMDEC,ftol

       pi=4.0d0*atan(1.0d0)
       rd=pi/180.0d0

! Initialize:

       ndim=np+1
       do 100 j1=1,np+1
        DP(j1)=0.1d0
100    continue
       if (pflg.eq.2 .or. pflg.eq.5) then
        DP(1)=0.0d0
        DP(np+1)=0.0d0
       endif
       if (pflg.eq.3 .or. pflg.eq.4 .or. pflg.eq.6) then
        DP(1)=0.0d0
       endif

       do 110 j1=1,ndim
        P(1,j1)=PC1(j1-1)
110    continue
       do 130 j1=2,ndim+1
        do 120 j2=1,ndim
         P(j1,j2)=P(1,j2)
120     continue
130    continue
       do 140 j1=1,ndim
        P(j1+1,j1)=P(j1+1,j1)-DP(j1)*P(j1+1,j1)
140    continue

! Compute the initial rms-values:

       do 210 j2=1,ndim+1
        do 200 j1=1,ndim
         PP(j1)=P(j2,j1)
200     continue
        Y(j2)=CHI2PMDEC(PP,THEC,F01,F01E,THE0,np,ndat,pflg)
!        print*,pflg,j2,Y(j2),sqrt(Y(j2)/(ndat+1))
210    continue
!       if (pflg.eq.3) stop

! Optimize:

       call AMPMDEC(P,Y,51,50,ndim,ftol,iter,THEC,F01,F01E,THE0,np,ndat,pflg)

! Compute spline fit:

       do 220 j1=1,np+1
        XP(j1)=THEC(j1-1)
        YPA(j1)=P(1,j1)
        PC1(j1-1)=P(1,j1)
220    continue



     
       call SPLINE(XP,YPA,np+1,0.0d0,0.0d0,YPA2)
  

       do 230 j1=0,ndat
        call SPLINT(XP,YPA,YPA2,np+1,THE0(j1),PC2(j1))
230    continue

       CHISQ(pflg)=Y(1)
       RMS(pflg)=0.0d0
       do 320 j1=0,ndat
        RMS(pflg)=RMS(pflg)+(F01(j1)-PC2(j1))**2
320    continue
       RMS(pflg)=sqrt(RMS(pflg)/(ndat+1))

! Compute matrix element for final scattering angles:

       do 330 j1=0,nthe
        call SPLINT(XP,YPA,YPA2,np+1,j1*pi/nthe,q)
        PCF1(pflg,j1)=q
330    continue
       end



! Included subroutines:

       include 'lib/pnrm.f'     ! Norm of the scattering phase function.

       include 'lib/ampmdec.f'   ! Nelder-Mead optimization for phase-matrix element.
       include 'lib/amtrypmdec.f'! Nelder-Mead optimization for phase-matrix element.
       include 'lib/chi2pmdec.f' ! chi2 for Nelder-Mead downhill optimization.
       include 'lib/gauleg.f'  ! Gauss-Legendre integration abscissae and weights.
       include 'lib/spline.f'  ! Spline interpolation.
       include 'lib/splint.f'  ! Spline interpolation.
    

