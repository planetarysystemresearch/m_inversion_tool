subroutine gauleg(a,b,xn,wt,n)
    use, intrinsic :: iso_fortran_env, only : real64,real128
    IMPLICIT NONE
    integer, parameter :: rk = real64
    integer, parameter :: qp = real128
    real(kind=rk), parameter :: pi = 4.0_rk*atan(1.0_rk)
    integer, intent(in) :: n
    real(kind=rk),intent(in) :: a,b             !lower and upper bound
    real(kind=rk), intent(out) :: xn(n),wt(n)   !abscissas, weights
    real(kind=qp) :: dx,x,pd,x0                 
    real(kind=qp), allocatable :: p(:)
    integer :: j1,j2,j3
    allocate(p(0:n))
    p(0) = 1.0_qp
    do j1=1,n
        x = cos(pi*(j1-0.25_qp)/(n+0.5_qp))  
        do j3=1,12
            p(1)=x
            do j2=1,n-1
                p(j2+1)=((2.0_qp*j2+1)*x*p(j2)-j2*p(j2-1))/(j2*1.0_qp+1)
            enddo
            pd=n*(x*p(n)-p(n-1))/(x**2-1.0_qp)
            x0=x            
            x=x-p(n)/pd
            x0=x-x0
            if(abs(x0)<10*epsilon(x0)) exit
        enddo
        xn(n+1-j1) = x*(b-a)/2.0_qp+(a+b)/2.0_qp
        wt(n+1-j1) = 2.0_qp/((1-x**2)*pd**2)*((b-a)/2.0_qp)
    enddo
    deallocate(p)
    if(mod(n,2)==1) then
        xn((n+1)/2)=(a+b)/2.0_rk   
    endif





end subroutine

