
      DOUBLE PRECISION FUNCTION amtrypmdec(p,y,psum,mp,np,ndim,ihi,fac, &
      &                         THEC,F01,F01E,THE0,npc,ndat,pflg)

      IMPLICIT NONE
      INTEGER ihi,mp,ndim,np,NMAX
      DOUBLE PRECISION fac,p(mp,np),psum(np),y(mp),chi2pmdec
      PARAMETER (NMAX=50)
!    USES chi2pmtrx1
      INTEGER j
      DOUBLE PRECISION fac1,fac2,ytry,ptry(NMAX)

      integer ndat,npc,pflg
      double precision THEC(0:360),F01(0:360),F01E(0:360),THE0(0:360)

      fac1=(1.0d0-fac)/ndim
      fac2=fac1-fac
      do 11 j=1,ndim
        ptry(j)=psum(j)*fac1-p(ihi,j)*fac2
11    continue
      ytry=chi2pmdec(ptry,THEC,F01,F01E,THE0,npc,ndat,pflg)

      if (ytry.lt.y(ihi)) then
        y(ihi)=ytry
        do 12 j=1,ndim
          psum(j)=psum(j)-p(ihi,j)+ptry(j)
          p(ihi,j)=ptry(j)
12      continue
      endif
      amtrypmdec=ytry
      return
      END
