
       subroutine PNRM(P,THE,pnorm,np,nint)

! Norm of the scattering phase function. 
!
! Author: Karri Muinonen
! Version: 2015 January 30

       implicit none
       integer np,nint,j1
       double precision P(0:360),THE(0:360),XP(361),YP(361),YP2(361)
       double precision  XI(512),WXI(512),pnorm,lnp

       call GAULEG(-1.0d0,1.0d0,XI,WXI,nint)

       do 10 j1=1,np+1
        XP(j1)=THE(j1-1)
!        YP(j1)=log(P(j1-1))
        YP(j1)=P(j1-1)
10     continue
       call SPLINE(XP,YP,np+1,0.0d0,0.0d0,YP2)
            
       pnorm=0.0d0
       do 20 j1=1,nint
        call SPLINT(XP,YP,YP2,np+1,acos(XI(j1)),lnp)
!        pnorm=pnorm+WXI(j1)*exp(lnp)
        pnorm=pnorm+WXI(j1)*lnp

20     continue

            end
