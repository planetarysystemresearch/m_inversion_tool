subroutine splint(xp,y,y2a,n,the,tmpP)
    !splint(tmpP,xp,coeffs,the)
    use, intrinsic :: iso_fortran_env, only: real64
    implicit none
    integer, parameter :: rk = REAL64
    integer, intent(in) :: n
    real(kind=rk), parameter :: pi = 4.0_rk*atan(1.0_rk)
    real(kind=rk), intent(in) :: xp(n),y(n),y2a(n) 
    real(kind=rk), intent(out) :: tmpP      !interpolated values
    real(kind=rk), intent(in)  :: the       !point where spline is evaluated
    real(kind=rk) :: tmpX
    integer :: j1,bisectionSearch
    real(kind=rk) :: coeffs(4),h
    if(the<xp(size(xp))) then
        !j1=1+int((the/pi)*(size(xp)-1.0_rk),kind=rk)
        j1 = bisectionSearch(xp,the,n)
        tmpX = the-xp(j1) 
    else
        tmpP=y(size(xp))
        return
    endif
   
    h = (xp(j1+1)-xp(j1))
    coeffs(1)=(y2a(j1+1)-y2a(j1))/(3.0_rk*h)
    coeffs(2)=y2a(j1)
    coeffs(3)=(y(j1+1)-y(j1))/h-(2.0_rk*y2a(j1)+y2a(j1+1))*h/3.0_rk 
    coeffs(4)=y(j1)

    tmpP=coeffs(1)*tmpX**3+coeffs(2)*tmpX**2+coeffs(3)*tmpX+coeffs(4)
  
end subroutine


!---------------------------------------------
!bisectionSearch(xp,the)
!Search index for the value(lower) closest to
!the value "the" using bisection search.
!
!IN: xp:    data   
!   the:    the value to look for
!---------------------------------------------
function bisectionSearch(xp,the,n) 
    use, intrinsic :: iso_fortran_env, only: real64
    implicit none
    integer, parameter :: rk = real64
    integer, intent(in) :: n
    real(kind=rk), parameter :: pi = 4.0_rk*atan(1.0_rk)
    real(kind=rk), intent(in) :: the         !point to be evaluated
    real(kind=rk), intent(in) :: xp(n)       !sorted array
    integer :: bisectionSearch
    integer :: x1,x2,xmid 
    x1=1
    x2=size(xp)
    do while(x2-x1>1)
        xmid=(x1+x2)/2
        if(xp(xmid) > the) then
            x2=xmid
        else
            x1=xmid
        endif
    enddo
    bisectionSearch=x1
end function





