
      SUBROUTINE ampmdec(p,y,mp,np,ndim,ftol,iter,THEC,F01,F01E,THE0,npc,ndat,pflg)

      IMPLICIT NONE
      INTEGER iter,mp,ndim,np,NMAX,ITMAX
      DOUBLE PRECISION ftol,p(mp,np),y(mp),chi2pmdec
      PARAMETER (NMAX=50,ITMAX=50000)
!    USES amtrypmdec,chi2pmdec
      INTEGER i,ihi,ilo,inhi,j,m,n
      DOUBLE PRECISION rtol,sum,swap,ysave,ytry,psum(NMAX),amtrypmdec

      integer ndat,npc,pflg
      double precision THEC(0:360),F01(0:360),F01E(0:360),THE0(0:360),pi,rd

      pi=4.0d0*atan(1.0d0)
      rd=pi/180.0d0

      iter=0
1     do 12 n=1,ndim
        sum=0.0d0
        do 11 m=1,ndim+1
          sum=sum+p(m,n)
11      continue
        psum(n)=sum
12    continue
2     ilo=1
      if (y(1).gt.y(2)) then
        ihi=1
        inhi=2
      else
        ihi=2
        inhi=1
      endif
      do 13 i=1,ndim+1
        if(y(i).le.y(ilo)) ilo=i
        if(y(i).gt.y(ihi)) then
          inhi=ihi
          ihi=i
        else if(y(i).gt.y(inhi)) then
          if(i.ne.ihi) inhi=i
        endif
13    continue
      rtol=2.0d0*abs(y(ihi)-y(ilo))/(abs(y(ihi))+abs(y(ilo)))

      if (rtol.lt.ftol) then
        swap=y(1)
        y(1)=y(ilo)
        y(ilo)=swap
        do 14 n=1,ndim
          swap=p(1,n)
          p(1,n)=p(ilo,n)
          p(ilo,n)=swap
14      continue
        print*,pflg,iter,y(1)
        return
      endif
      if (iter.ge.ITMAX) stop 'ITMAX exceeded in amoeba'
      iter=iter+2

      ytry=amtrypmdec(p,y,psum,mp,np,ndim,ihi,-1.0d0, &
     &                THEC,F01,F01E,THE0,npc,ndat,pflg)

!      print*,iter,ytry,y(ilo)


      if (ytry.le.y(ilo)) then

        ytry=amtrypmdec(p,y,psum,mp,np,ndim,ihi,2.0d0, &
     &                  THEC,F01,F01E,THE0,npc,ndat,pflg)

      else if (ytry.ge.y(inhi)) then
        ysave=y(ihi)

        ytry=amtrypmdec(p,y,psum,mp,np,ndim,ihi,0.5d0, &
     &                  THEC,F01,F01E,THE0,npc,ndat,pflg)

        if (ytry.ge.ysave) then
          do 16 i=1,ndim+1
            if(i.ne.ilo)then
              do 15 j=1,ndim
                psum(j)=0.5d0*(p(i,j)+p(ilo,j))
                p(i,j)=psum(j)
15            continue

              y(i)=chi2pmdec(psum,THEC,F01,F01E,THE0,npc,ndat,pflg)

            endif
16        continue
          iter=iter+ndim
          goto 1
        endif
      else
        iter=iter-1
      endif
      goto 2
      END

