
subroutine spline(xp,y,n,yp1,ypn,y2)
    !cubicspline(xp,coeffs,dfa,dfb)
    use, intrinsic :: iso_fortran_env, only: real64
    implicit none
    integer, parameter :: rk = REAL64
    integer, intent(in) :: n
    real(kind=rk), intent(in) :: yp1,ypn
    real(kind=rk), intent(out) :: y2(n)           !x-axis data
    real(kind=rk),intent(in) :: xp(n),y(n)
    real(kind=rk) :: coeffs(4,1,n)   !coeffs, coeffs(4,:,:) should contain y-axis data
    real(kind=rk) :: dfa,dfb      !derivatives at the end points                                
    real(kind=rk),allocatable :: h(:),a(:),aa(:),bb(:),cc(:),x(:)
    integer :: j1,j2


    dfa = yp1
    dfb = ypn

    coeffs(4,1,:) = y(:)
    

    allocate(h(n),a(n),aa(n),bb(n),cc(n),x(n))
    do j1=1,n-1
        h(j1)=xp(j1+1)-xp(j1)
    enddo
    do j2=1,size(coeffs,dim=2)
        a(1)=3.0_rk*(coeffs(4,j2,2)-coeffs(4,j2,1))/h(1)-3.0_rk*dfa
        a(n)=3.0_rk*dfb-3.0_rk*(coeffs(4,j2,n)-coeffs(4,j2,n-1))/h(n-1)

        do j1=2,n-1       
            a(j1)=3.0_rk*(coeffs(4,j2,j1+1)-coeffs(4,j2,j1))/h(j1)&
            &   -3.0_rk*(coeffs(4,j2,j1)-coeffs(4,j2,j1-1))/h(j1-1)
        enddo

        do j1=1,n-1
            aa(j1)=h(j1)
            cc(j1)=h(j1)
        enddo

        bb(1) = 2.0_rk*h(1)
        do j1=2,n-1
            bb(j1)=2.0_rk*(h(j1)+h(j1-1))
        enddo
        bb(n) = 2.0_rk*h(n-1)
        call thomasAlgorithm(x,aa,bb,cc,a,n)
        y2(:)= x(:) 
    enddo


   

    deallocate(h,a,aa,bb,cc)
end subroutine



    !---------------------------------------------
    !thomasAlgorithm(x,a0,b0,c0,d0)
    !solves tridiagonal matrix
    !
    !IN: a0,b0,c0:  diagonal vectors
    !          d0:  right side column vector
    !OUT:       x:  result
    !--------------------------------------------
    subroutine thomasAlgorithm(x,a0,b0,c0,d0,n)
        use, intrinsic :: iso_fortran_env, only: real64
        implicit none
        integer, parameter :: rk = REAL64
        integer, intent(in) :: n
        real(kind=rk), intent(in) :: a0(n),b0(n),c0(n),d0(n)
        real(kind=rk), intent(out) :: x(n)
        real(kind=rk), allocatable :: c(:),d(:)
        integer :: j1
        allocate(c(n),d(n))
        c(1) = c0(1)/b0(1)
        do j1=2,n-1
            c(j1)=c0(j1)/(b0(j1)-a0(j1-1)*c(j1-1))
        enddo
        d(1)=d0(1)/b0(1)
        do j1=2,n
            d(j1)=(d0(j1)-a0(j1-1)*d(j1-1))/(b0(j1)-a0(j1-1)*c(j1-1))
        enddo
        x(n)=d(n)
        do j1=n-1,1,-1
            x(j1)=d(j1)-c(j1)*x(j1+1)
        enddo
        deallocate(c,d)
    end subroutine
