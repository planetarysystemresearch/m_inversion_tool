
       double precision function CHI2PMDEC(P,THEC,F01,F01E,THE0,np,ndat,pflg)

! Computation of the chi2 between the model and data scattering phase matrix 
! elements.
!
! Author: Karri Muinonen
! Version: 2019 January 7
 
       implicit none
       integer j1,np,ndat,pflg
       double precision P(50),THEC(0:360),F01(0:360),F01E(0:360)
       double precision THE0(0:360),PC(0:360)
       double precision XP(361),YPA(361),YPA2(361)

! Compute model phase matrix element:

       do 10 j1=1,np+1
        XP(j1)=THEC(j1-1)
        YPA(j1)=P(j1)
10     continue
       call SPLINE(XP,YPA,np+1,0.0d0,0.0d0,YPA2)
       do 20 j1=0,ndat
        call SPLINT(XP,YPA,YPA2,np+1,THE0(j1),PC(j1))
20     continue

! Sanity checks:

       if (pflg.eq.1) then

        do 30 j1=1,np+1
         if (P(j1).lt.0.0d0) then
          CHI2PMDEC=1.0d12
          return
         endif
30      continue

        do 40 j1=0,ndat
         if (PC(j1).lt.0.0d0) then
          CHI2PMDEC=1.0d12
          return
         endif
40      continue

       else

        do 50 j1=1,np+1
         if (abs(P(j1)).gt.1.001d0) then
          CHI2PMDEC=1.0d12
          return
         endif
50      continue

        do 60 j1=0,ndat
         if (abs(PC(j1)).gt.1.001d0) then
          CHI2PMDEC=1.0d12
          return
         endif
60      continue

       endif

! Chi2-value:

       CHI2PMDEC=0.0d0
       do 70 j1=0,ndat
        CHI2PMDEC=CHI2PMDEC+(F01(j1)-PC(j1))**2/F01E(j1)**2
70     continue
       end

